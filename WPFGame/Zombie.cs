﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;

namespace WPFGame
{
    public class Zombie : BaseCharacter
    {
        public BaseEntity Enemy { get; set; }

        public Zombie
        (
        Point origin = new Point(),
        Size size = new Size(),
        Vector velocity = new Vector(),
        Sprite sprite = null,
        BaseEntity enemy = null
        ) : base(origin, size, velocity, sprite)
        {
            this.Enemy = enemy;
            this.MovementSpeed = 32;
            this.Name = "zombie";
        }

        public override void Think()
        {
            if (this.Enemy == null)
                return;

            this.MovementFlags = FLAGS.FL_NONE;

            Vector direction = new Vector();
            direction.X = this.Enemy.Origin.X - Origin.X;
            direction.Y = this.Enemy.Origin.Y - Origin.Y;

            if (direction.X == 0 && direction.Y == 0)
                return;

            if (direction.X > 0)
                this.MovementFlags |= FLAGS.FL_MOVERIGHT;
            else
                this.MovementFlags |= FLAGS.FL_MOVELEFT;

            if (direction.Y > 0)
                this.MovementFlags |= FLAGS.FL_MOVEUP;
            else
                this.MovementFlags |= FLAGS.FL_MOVEDOWN;

            base.Think();
        }
    }
}
