﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;

namespace WPFGame
{
    class Coin : BaseEntity
    {
        public Player Owner { get; set; }

        public Coin
        (
            Point origin = new Point(),
            Size size = new Size(),
            Vector velocity = new Vector(),
            Sprite sprite = null
        ) : base(origin, size, velocity, sprite)
        {
            this.Solid = true;
        }

        public void Pick(Player player)
        {
            this.Owner = player;
            this.Solid = false;
            this.Visible = false;
            player.Score++;
        }
    }
}
