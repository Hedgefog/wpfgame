﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFGame
{
    public static class Constants
    {
        public enum ResourceType
        {
            RT_GENERIC = 0,
            RT_SPRITE,
            RT_IMAGE,
            RT_SOUND
        }

        public const int MAX_ENTITIES = 512;
        public const int MAX_PRECACHED_RESOURCES = 512;
        public const int MAX_HUD_ELEMENTS = 32;
        public const int BLOCK_SIZE = 72;
        public const bool DEBUG_ENABLED = true;
    }
}
