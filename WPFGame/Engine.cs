﻿using System;
using System.Windows;
using System.Windows.Media;
using System.Windows.Threading;
using System.Windows.Controls;
using System.Windows.Shapes;
using System.Runtime.InteropServices;

using System.Windows.Media.Imaging;

namespace WPFGame
{
    public class Engine
    {
        [DllImport("Kernel32")]
        public static extern void AllocConsole();

        public event EventHandler EventThink;

        public World World { get; protected set; }
        public Render Render { get; protected set; }

        protected int m_startTick; //Engine start time
        protected DispatcherTimer m_timer; //Think timer
        protected Sprite[] m_sprites; //Precached sprites
        protected ImageSource[] m_textures; //Precached textures

        public Engine()
        {
            AllocConsole(); //Console allocation

            m_startTick = Environment.TickCount; //Get start time

            this.Render = new Render();
            this.World = new World();

            m_sprites = new Sprite[Constants.MAX_PRECACHED_RESOURCES];
            m_textures = new ImageSource[Constants.MAX_PRECACHED_RESOURCES];

            m_timer = new DispatcherTimer();
            m_timer.Tick += (sender, e) => Think();
            m_timer.Interval = TimeSpan.FromMilliseconds(1);
            m_timer.Start();
        }

        //Engine frame (Think)
        public void Think()
        {
            this.Render.ClearFrame();

            //Draw blocks
            for (int column = 0; column < this.World.Map.Columns; column++)
            {
                for (int row = 0; row < this.World.Map.Rows; row++)
                {
                    Box box = this.World.Map.GetBox(row, column);

                    if (box.Right < this.Render.StartOrigin.X ||
                        box.Bottom > this.Render.StartOrigin.Y ||
                        box.Left > this.Render.StartOrigin.X + this.Render.Canvas.Width ||
                        box.Top < this.Render.StartOrigin.Y - this.Render.Canvas.Height)
                    {
                        continue;
                    }

                    Map.Block block = this.World.Map.GetBlock(row, column);
                    int texture = this.World.Map.GetBlockTexture(block);
                    ImageSource imageSource = GetTexture(texture);

                    if (imageSource == null)
                        continue;

                    double scale = box.Width / imageSource.Width;
                    this.Render.DrawImage(imageSource, box.Origin, scale);
                    
                    if (block == Map.Block.BL_WALL)
                        this.Render.DrawBox(box, Brushes.Green);
                }
            }

            //Draw entities
            for (int i = 0; i < Constants.MAX_ENTITIES; i++)
            {
                BaseEntity entity = this.World.Entities[i];

                if (entity == null)
                    continue;

                entity.Think();

                //Apply velocity
                int tickCount = Environment.TickCount;
                if (entity.Velocity.X != 0 || entity.Velocity.Y != 0)
                {
                    if (tickCount > entity.LastTick)
                    {    
                        int ticks = (tickCount - entity.LastTick);

                        //Calculate new position
                        Point origin = entity.Origin;
                        origin.X += entity.Velocity.X / 1000 * ticks;
                        origin.Y += entity.Velocity.Y / 1000 * ticks;

                        //Move entity to new origin
                        MoveEntity(entity, origin);
                    }
                }

                //Save last think time
                entity.LastTick = tickCount;

                //Draw entity at canvas
                if (entity.Visible)
                    this.Render.DrawEntity(entity);

                //Draw collision box at canvas
                if (Constants.DEBUG_ENABLED)
                {
                    this.Render.DrawCollisionBox(entity);

                    if (entity.Name == "player")
                    {
                        this.Render.DrawMessage(string.Format("{0} {1}", entity.Velocity.X, entity.Velocity.Y), 
                                                new Point(8, 64), Brushes.Red);

                        this.Render.DrawMessage(string.Format("{0} {1}", entity.Origin.X, entity.Origin.Y), 
                                                new Point(8, 96), Brushes.Red);
                    }
                }

                //Move camera
                if (entity.Name == "player")
                    this.Render.StartOrigin = new Point(entity.Origin.X - this.Render.Canvas.Width / 2,
                        entity.Origin.Y + this.Render.Canvas.Height / 2);
            }

            if (Constants.DEBUG_ENABLED)
                this.Render.DrawAxis();

            if (EventThink != null)
                EventThink(this, EventArgs.Empty);
        }

        //Function move entity with collision detection
        public void MoveEntity(BaseEntity entity, Point origin)
        {
            Vector direction = new Vector();
            direction.X = origin.X - entity.Origin.X;
            direction.Y = origin.Y - entity.Origin.Y;

            if (direction.X == 0.0 && direction.Y == 0.0)
                return;
            
            if (entity.Solid)
            {  
                //Get collision box of entity
                Box collideBox = new Box();
                collideBox.X = origin.X;
                collideBox.Y = origin.Y;
                collideBox.Size = entity.Size;

                Box sweptBox = Box.GetSweptBox(collideBox, direction);

                if (Constants.DEBUG_ENABLED)
                    this.Render.DrawBox(sweptBox, Brushes.DarkOrange);

                double lowesFraction = double.PositiveInfinity;
                BaseEntity touchTarget = null;

                for (int column = 0; column < this.World.Map.Columns; column++)
                {
                    for (int row = 0; row < this.World.Map.Rows; row++)
                    {
                        if (this.World.Map.CheckSolid(row, column))
                        {
                            Box box = this.World.Map.GetBox(row, column);

                            if (sweptBox.Intersect(box))
                            {
                                double fraction;
                                Vector _direction = collideBox.Swept(box, direction, out fraction);

                                if (fraction < lowesFraction)
                                {
                                    lowesFraction = fraction;
                                    direction = _direction;
                                }

                                if (Constants.DEBUG_ENABLED && entity.Name == "player")
                                    this.Render.DrawBox(box, Brushes.Red);
                            }
                        }
                    }
                }

                for (int i = 0; i < Constants.MAX_ENTITIES; i++)
                {
                    BaseEntity _entity = this.World.Entities[i]; //Target entity

                    if (_entity == null)
                        continue;

                    if (_entity == entity)
                        continue;

                    if (!_entity.Solid)
                        continue;

                    //Get collision box of target entity
                    Box _collideBox = new Box();
                    _collideBox.X = _entity.Origin.X;
                    _collideBox.Y = _entity.Origin.Y;
                    _collideBox.Size = _entity.Size;
                    
                    if (sweptBox.Intersect(_collideBox))
                    {
                        double fraction;
                        Vector _direction = collideBox.Swept(_collideBox, direction, out fraction);

                        if (fraction < lowesFraction)
                        {
                            lowesFraction = fraction;
                            direction = _direction;
                            touchTarget = _entity;
                        }
                    }
                }

                //Call touch event
                if (touchTarget != null)
                {
                    entity.Touch(touchTarget);
                    touchTarget.Touch(entity);
                }

                //Move entity to new origin
                origin.X = entity.Origin.X + direction.X;
                origin.Y = entity.Origin.Y + direction.Y;
            }

            entity.Origin = origin;
        }

        //Return precached sprite by index
        public Sprite GetSprite(int index)
        {
            return m_sprites[index];
        }

        //Return precached texture by index
        public ImageSource GetTexture(int index)
        {
            return m_textures[index];
        }

        //Precache sprite by name
        public int PrecacheSprite(string name)
        {
            for (int i = 0; i < Constants.MAX_PRECACHED_RESOURCES; i++)
            {
                if (m_sprites[i] == null)
                {
                    m_sprites[i] = new Sprite(name);
                    return i;
                }
            }

            return -1;
        }

        //Precache texture by name
        public int PrecacheTexture(string name)
        {
            for (int i = 0; i < Constants.MAX_PRECACHED_RESOURCES; i++)
            {
                if (m_textures[i] == null)
                {
                    BitmapImage bitmap = new BitmapImage(new Uri("assets\\textures\\" + name + ".png", UriKind.Relative));
                    Int32Rect rect = new Int32Rect();
                    rect.Width = (int)Math.Round(bitmap.Width);
                    rect.Height = (int)Math.Round(bitmap.Height);
                    m_textures[i] = new CroppedBitmap(bitmap, rect);
                    return i;
                }
            }

            return -1;
        }
    }
}
