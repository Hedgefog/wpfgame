﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;

namespace WPFGame
{
    public struct Box
    {
        public Point Origin { get; set; }
        public Size Size { get; set; }

        public double Right
        {
            get { return this.Origin.X + this.Size.Width / 2; }
        }

        public double Left
        {
            get { return this.Origin.X - this.Size.Width / 2; }
        }

        public double Top
        {
            get { return this.Origin.Y + this.Size.Height / 2; }
        }

        public double Bottom
        {
            get { return this.Origin.Y - this.Size.Height / 2; }
        }

        public double Width
        {
            get { return this.Size.Width; }
            set { this.Size = new Size(value, this.Height); }
        }

        public double Height
        {
            get { return this.Size.Height; }
            set { this.Size = this.Size = new Size(this.Width, value); }
        }

        public double X
        {
            get { return this.Origin.X; }
            set { this.Origin = new Point(value, this.Origin.Y); }
        }

        public double Y
        {
            get { return this.Origin.Y; }
            set { this.Origin = new Point(this.Origin.X, value); }
        }

        public Box(Point origin, Size size)
        {
            this.Origin = origin;
            this.Size = size;
        }

        public Box(double x, double y, double width, double height)
        {
            this.Origin = new Point(x, y);
            this.Size = new Size(width, height);
        }

        public static Box Empty { get; }

        public bool Intersect(Box target)
        {
            if (this.Right < target.Left)
                return false;

            if (this.Top < target.Bottom)
                return false;

            if (this.Bottom > target.Top)
                return false;

            if (this.Left > target.Right)
                return false;

            return true;
        }
        
        public static Box GetSweptBox(Box box, Vector direction)
        {
            Box sweptBox = new Box(box.X, box.Y, 0, 0);

            if (direction.X <= 0)
                sweptBox.X += direction.X;

            if (direction.Y <= 0)
                sweptBox.Y += direction.Y;
            
            sweptBox.Width = direction.X > 0 ? direction.X + box.Width : box.Width - direction.X;
            sweptBox.Height = direction.Y > 0 ? direction.Y + box.Height : box.Height - direction.Y;

            return sweptBox;
        }

        public Vector Swept(Box target, Vector direction, out double fraction)
        {
            Point invEntry = new Point(0 ,0);
            Point invExit = new Point(0, 0);
            
            if (direction.X > 0.0)
            {
                invEntry.X = target.X - (this.Origin.X + this.Width);
                invExit.X = (target.Origin.X + target.Width) - this.Origin.X;
            }
            else
            {
                invEntry.X = (target.Origin.X + target.Width) - this.Origin.X;
                invExit.X = target.X - (this.Origin.X + this.Width);
            }

            if (direction.Y > 0.0)
            {
                invEntry.Y = target.Origin.Y - (this.Origin.Y + this.Height);
                invExit.Y = (target.Origin.Y + target.Height) - this.Origin.Y;
            }
            else
            {
                invEntry.Y = (target.Origin.Y + target.Height) - this.Origin.Y;
                invExit.Y = target.Origin.Y - (this.Origin.Y + this.Height);
            }

            Point entry = new Point(double.NegativeInfinity, double.NegativeInfinity);
            Point exit = new Point(double.PositiveInfinity, double.PositiveInfinity);

            if (direction.X != 0.0)
            {
                entry.X = invEntry.X / direction.X;
                exit.X = invExit.X / direction.X;
            }

            if (direction.Y != 0.0)
            {
                entry.Y = invEntry.Y / direction.Y;
                exit.Y = invExit.Y / direction.Y;
            }

            Vector normal = new Vector(0, 0);
            double max = Math.Max(entry.X, entry.Y);
            double min = Math.Min(exit.X, exit.Y);
            if (max > min || entry.X < 0.0 && entry.Y < 0.0
                || entry.X > 1.0 || entry.Y > 1.0)
            {
                fraction = 1.0;
            }
            else
            {
                fraction = max;

                if (entry.X > entry.Y)
                {
                    if (invEntry.X < 0.0)
                        normal.X = 1.0;
                    else
                        normal.X = -1.0;
                }
                else
                {
                    if (invEntry.Y < 0.0)
                        normal.Y = 1.0;
                    else
                        normal.Y = -1.0;
                }
            }

            direction = Vector.Multiply((1.0 - fraction), direction);

            return direction;
        }
    }
}
