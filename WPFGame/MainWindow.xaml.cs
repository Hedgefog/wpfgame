﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Threading;

namespace WPFGame
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //Engine
        protected Engine m_engine;

        //Entities
        protected Player m_player;
        protected Zombie m_zombie;

        //Sprites
        protected int m_sprPlayer;
        protected int m_sprZombie;
        protected int m_sprGround;
        protected int m_sprWall;
        protected int m_sprCoin;

        //Textures
        protected int m_textureWall;
        protected int m_textureGround;

        //Map
        Map m_map;
        int m_mapRows;
        int m_mapColumns;

        //Game
        DispatcherTimer m_coinTimer;
        int m_coinCount;

        public MainWindow()
        {
            InitializeComponent();

            this.Width = 800;
            this.Height = 600;
            GameCanvas.Width = this.Width;
            GameCanvas.Height = this.Height;

            //Engine Init
            m_engine = new Engine();
            m_engine.Render.Canvas = GameCanvas;
            m_engine.Render.StartOrigin = new Point(-512, 512);

            //Map Init
            m_mapRows = 16;
            m_mapColumns = 16;
            m_map = new Map(m_mapRows, m_mapColumns, m_engine.Render.StartOrigin);
            m_engine.World.Map = m_map;

            //Precache assets
            Console.WriteLine("Precaching resources...");
            m_sprPlayer = m_engine.PrecacheSprite("player");
            m_sprZombie = m_engine.PrecacheSprite("zombie_walk");
            m_sprWall = m_engine.PrecacheSprite("wall");
            m_sprCoin = m_engine.PrecacheSprite("coin");

            m_textureWall = m_engine.PrecacheTexture("Wall");
            m_textureGround = m_engine.PrecacheTexture("Ground");

            //Player entity init
            m_player = new Player(new Point(), new Size(24, 64));
            m_player.Sprite = m_engine.GetSprite(m_sprPlayer);

            //Zombie entity init
            m_zombie = new Zombie(new Point(), new Size(24, 64));
            m_zombie.Enemy = m_player;
            m_zombie.Sprite = m_engine.GetSprite(m_sprZombie);

            //Spawn entity
            Console.WriteLine("Spawn entities...");
            m_engine.World.AddEntity(m_player);
            m_engine.World.AddEntity(m_zombie);

            //Set texture of the map block
            m_map.SetBlockTexture(Map.Block.BL_WALL, m_textureWall);
            m_map.SetBlockTexture(Map.Block.BL_GROUND, m_textureGround);

            //Generate new map
            Console.WriteLine("Generating map...");
            GenerateMap();

            m_player.Origin = m_map.GetBoxOrigin(1, 1);
            m_zombie.Origin = m_map.GetBoxOrigin(m_mapRows - 2, m_mapColumns - 2);

            //Setup key handler (character control)
            this.KeyDown += new KeyEventHandler(OnButtonKeyDown);
            this.KeyUp += new KeyEventHandler(OnButtonKeyUp);
            
            this.SizeChanged += new SizeChangedEventHandler(onWindowSizeChanged);

            Console.WriteLine("Loaded!\n\n\n\n");

            m_coinCount = 0;

            m_coinTimer = new DispatcherTimer();
            m_coinTimer.Interval = TimeSpan.FromSeconds(1);
            m_coinTimer.Tick += (sender, e) => SpawnCoin();
            m_coinTimer.Start();

            //Print map to console
            if (Constants.DEBUG_ENABLED)
            {
                Console.WriteLine("Map:\n");
                m_map.Print();
            }
        }

        public void SpawnCoin()
        {
            if (m_coinCount > 10)
                return;

            Random random = new Random();
            int row = random.Next(1, m_map.Rows - 1);
            int column = random.Next(1, m_map.Columns - 1);

            for (int i = column; column < m_map.Columns; i++)
            {
                if (i >= (m_map.Columns - 1))
                    i = 0;

                bool stopLoop = false;
                for (int j = row; j < m_map.Rows; j++)
                {
                    if (j >= (m_map.Rows - 1))
                        j = 0;

                    if (m_map.GetBlock(j, i) == Map.Block.BL_WALL)
                        continue;

                    int randomNum = random.Next(100);

                    if(randomNum > 20 && randomNum < 50)
                    {
                        row = j;
                        column = i;
                        stopLoop = true;
                        break;
                    }
                }

                if (stopLoop)
                    break;
            }

            Point origin = m_map.GetBoxOrigin(row, column);

            Coin coin = new Coin();
            coin.Sprite = m_engine.GetSprite(m_sprCoin);
            coin.Origin = origin;
            coin.Size = new Size(16, 16);

            coin.EventTouch += onCoinTouch;

            m_engine.World.AddEntity(coin);

            m_coinCount++;

            if (Constants.DEBUG_ENABLED)
                Console.WriteLine("Spawn coin at {0}", coin.Origin);
        }

        public void onCoinTouch(object sender, BaseEntity.EventTouchArgs e)
        {
            Coin coin = (Coin)e.Target;

            if (e.Toucher.Name != "player")
                return;

            Player player = (Player)e.Toucher;
            
            coin.Pick(player);

            m_engine.World.RemoveEntity(coin);

            m_coinCount--;
        }

        protected void onWindowSizeChanged(object sender, System.EventArgs e)
        {
            GameCanvas.Width = this.Width;
            GameCanvas.Height = this.Height;
        }

        protected void OnButtonKeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Right:
                case Key.D:
                    m_player.MovementFlags |= BaseCharacter.FLAGS.FL_MOVERIGHT;
                    break;
                case Key.Left:
                case Key.A:
                    m_player.MovementFlags |= BaseCharacter.FLAGS.FL_MOVELEFT;
                    break;
                case Key.Up:
                case Key.W:
                    m_player.MovementFlags |= BaseCharacter.FLAGS.FL_MOVEUP;
                    break;
                case Key.Down:
                case Key.S:
                    m_player.MovementFlags |= BaseCharacter.FLAGS.FL_MOVEDOWN;
                    break;
            }
        }

        protected void OnButtonKeyUp(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Right:
                case Key.D:
                    m_player.MovementFlags &= ~BaseCharacter.FLAGS.FL_MOVERIGHT;
                    break;
                case Key.Left:
                case Key.A:
                    m_player.MovementFlags &= ~BaseCharacter.FLAGS.FL_MOVELEFT;
                    break;
                case Key.Up:
                case Key.W:
                    m_player.MovementFlags &= ~BaseCharacter.FLAGS.FL_MOVEUP;
                    break;
                case Key.Down:
                case Key.S:
                    m_player.MovementFlags &= ~BaseCharacter.FLAGS.FL_MOVEDOWN;
                    break;
            }
        }

        protected void GenerateMap()
        {
            if (m_engine.World.Map == null)
                return;
            
            Random random = new Random();

            //Pattern
            for (int i = 0; i < m_mapRows; i++)
            {
                for (int j = 0; j < m_mapColumns; j++)
                {
                    if (((i == 1 || i == m_mapRows - 2) && j > 0 && j < m_mapColumns - 1) || 
                        ((j == 1 || j == m_mapColumns - 2) && i > 0 && i < m_mapRows - 1))
                    {
                        m_engine.World.Map.SetBlock(i, j, Map.Block.BL_GROUND);
                    }
                    else
                    {
                        m_engine.World.Map.SetBlock(i, j, Map.Block.BL_WALL);
                    }
                }
            }

            //Generation %)
            for (int i = 0; i < 4; i++)
            {
                int directionX = 0;
                int directionY = 0;

                if (i == 1 || i == 3)
                    directionX = 1;

                if (i == 2 || i == 3)
                    directionY = 1;

                int x = 0;
                if (directionX == 1)
                    x = 2;
                else
                    x = m_mapRows - 2;

                int y = 0;
                if (directionY == 1)
                    y = 2;
                else
                    y = m_mapColumns - 2;

                while (true)
                {
                    int way = random.Next(100);

                    if (way > 50)
                    {
                        if (directionX == 1)
                            x++;
                        else
                            x--;
                    }
                    else
                    {
                        if (directionY == 1)
                            y++;
                        else
                            y--;
                    }

                    if (directionX == 1)
                    {
                        if (x >= m_mapRows - 1)
                            break;
                    }
                    else
                    {
                        if (x <= 1)
                            break;
                    }

                    if (directionY == 1)
                    {
                        if (y >= m_mapColumns - 1)
                            break;
                    }
                    else
                    {
                        if (y <= 1)
                            break;
                    }

                    m_engine.World.Map.SetBlock(x, y, Map.Block.BL_GROUND);
                }
            }
        }
    }
}
