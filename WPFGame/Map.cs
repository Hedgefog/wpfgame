﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;

namespace WPFGame
{
    public class Map
    {
        public enum Block
        {
            BL_NONE,
            BL_GROUND,
            BL_WALL,

            BL_COUNT
        }

        public int Rows
        {
            get { return m_map.GetLength(0); }
        }

        public int Columns
        {
            get { return m_map.GetLength(1); }
        }

        public const int BLOCK_SIZE = 72;

        protected int[,] m_map;
        protected int[] m_blockTexture;
        protected Point m_startOrigin;

        public Map(int rows, int columns, Point startOrigin)
        {
            m_startOrigin = startOrigin;

            m_map = new int[rows, columns];
            m_blockTexture = new int[(int)Block.BL_COUNT];
        }

        public void SetBlock(int row, int column, Block block)
        {
            m_map[row, column] = (int)block;
        }

        public Block GetBlock(int row, int column)
        {
            return (Block)m_map[row, column];
        }

        public void SetBlockTexture(Block block, int texture)
        {
            m_blockTexture[(int)block] = texture;
        }

        public int GetBlockTexture(Block block)
        {
            return m_blockTexture[(int)block];
        }

        public int GetRow(Point point)
        {
            return (int)Math.Floor((m_startOrigin.Y - point.Y) / Map.BLOCK_SIZE);
        }

        public int GetColumn(Point point)
        {
            return (int)Math.Ceiling((-m_startOrigin.X + point.X) / Map.BLOCK_SIZE);
        }

        public bool CheckSolid(int row, int column)
        {
            if (m_map[row, column] == (int)Block.BL_WALL)
                return true;
            
            return false;
        }

        public Box GetBox(int row, int column)
        {
            Box box = new Box();
            box.Width = Map.BLOCK_SIZE;
            box.Height = Map.BLOCK_SIZE;
            box.X = m_startOrigin.X + (column * Map.BLOCK_SIZE);
            box.Y = m_startOrigin.Y - (row * Map.BLOCK_SIZE);

            return box;
        }

        public Point GetBoxOrigin(int row, int column)
        {
            Point origin = new Point();
            origin.X = m_startOrigin.X + (column * Map.BLOCK_SIZE);
            origin.Y = m_startOrigin.Y - (row * Map.BLOCK_SIZE);

            return origin;
        }

        public void Print()
        {
            for (int i = 0; i < Rows; i++)
            {
                for (int j = 0; j < Columns; j++)
                {
                    Block block = GetBlock(i, j);

                    if (block == Block.BL_WALL)
                        Console.Write("[]");
                    else
                        Console.Write("  ");
                }

                Console.Write("\n");
            }
        }
    }
}
