﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;

namespace WPFGame
{
    public class BaseCharacter : BaseEntity
    {
        public enum FLAGS
        {
            FL_NONE = 0x0,
            FL_MOVERIGHT = 0x1,
            FL_MOVELEFT = 0x2,
            FL_MOVEUP = 0x4,
            FL_MOVEDOWN = 0x8
        }

        public FLAGS MovementFlags { get; set; }
        public double MovementSpeed { get; set; }

        public BaseCharacter
        (
            Point origin = new Point(),
            Size size = new Size(),
            Vector velocity = new Vector(),
            Sprite sprite = null
        ) : base(origin, size, velocity, sprite)
        {
            //...
        }

        //Method for check some flag
        public bool IsMovementFlagSet(FLAGS flag)
        {
            if ((this.MovementFlags & flag) == flag)
                return true;

            return false;
        }

        public override void Think()
        {
            Vector velocity = new Vector(0, 0);

            if (IsMovementFlagSet(FLAGS.FL_MOVELEFT))
                velocity.X -= this.MovementSpeed;

            if (IsMovementFlagSet(FLAGS.FL_MOVERIGHT))
                velocity.X += this.MovementSpeed;

            if (IsMovementFlagSet(FLAGS.FL_MOVEUP))
                velocity.Y += this.MovementSpeed;

            if (IsMovementFlagSet(FLAGS.FL_MOVEDOWN))
                velocity.Y -= this.MovementSpeed;

            if (IsMovementFlagSet(FLAGS.FL_MOVEUP) || IsMovementFlagSet(FLAGS.FL_MOVEDOWN))
                velocity.X /= 2;

            if (IsMovementFlagSet(FLAGS.FL_MOVERIGHT) || IsMovementFlagSet(FLAGS.FL_MOVELEFT))
                velocity.Y /= 2;

            this.Velocity = velocity;

            if (this.Sprite != null)
            {
                if (this.Velocity.X == 0 && this.Velocity.Y == 0)
                {
                    if (m_spriteTimer.IsEnabled)
                    {
                        this.SpriteFrame = 0;
                        m_spriteTimer.Stop();
                    }
                }
                else
                {
                    if (!m_spriteTimer.IsEnabled)
                        m_spriteTimer.Start();
                }
            }

            base.Think(); //Call base Think
        }
    }
}
