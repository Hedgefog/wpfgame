﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Threading;

namespace WPFGame
{
    public class BaseEntity
    {
        public class EventTouchArgs : EventArgs
        {
            public BaseEntity Target { get; set; }
            public BaseEntity Toucher { get; set; }
        }
        protected DispatcherTimer m_spriteTimer; //Frame timer for entity sprite

        public event EventHandler EventThink;
        public event EventHandler<EventTouchArgs> EventTouch;

        public Point Origin { get; set; }
        public Size Size { get; set; }
        public Vector Velocity { get; set; }
        public Sprite Sprite { get; set; }
        public int LastTick { get; set; }
        public int SpriteFrame { get; set; }
        public bool Solid { get; set; }
        public bool Visible { get; set; }
        public string Name { get; protected set; }

        public BaseEntity
        (
            Point origin = new Point(), 
            Size size = new Size(), 
            Vector velocity = new Vector(),
            Sprite sprite = null
        )
        {   
            //Set default values
            this.Solid = true;
            this.Visible = true;
            this.SpriteFrame = 0;

            //Save data from arguments
            this.Origin = origin;
            this.Size = size;
            this.Velocity = velocity;
            this.Sprite = sprite;

            //Init timer
            m_spriteTimer = new DispatcherTimer();
            m_spriteTimer.Tick += (sender, e) => NextSpriteFrame();
            m_spriteTimer.Start();

            //Entity init complete
            this.LastTick = Environment.TickCount; //Get time
        }

        public void NextSpriteFrame()
        {
            if (this.Sprite == null)
                return;

            double duration = this.Sprite.GetFrame(this.SpriteFrame).Duration;

            if (duration <= 0)
                m_spriteTimer.Stop();

            this.SpriteFrame++;

            if (this.SpriteFrame >= this.Sprite.FrameCount)
                this.SpriteFrame = 0;

            m_spriteTimer.Interval = TimeSpan.FromMilliseconds(duration);
        }

        //This function call by engine every frame
        public virtual void Think()
        {
            if(EventThink != null)
                EventThink(this, null);
        } 

        public virtual void Touch(BaseEntity toucher)
        {
            EventTouchArgs args = new EventTouchArgs();
            args.Target = this;
            args.Toucher = toucher;

            if (EventTouch != null)
                EventTouch(this, args);
        }
    }
}
