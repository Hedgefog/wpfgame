﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WPFGame
{
    public class World
    {
        public BaseEntity[] Entities
        {
            get { return m_entities; }
        }

        public Map Map { get; set; }
        
        protected BaseEntity[] m_entities;

        public World()
        {
            m_entities = new BaseEntity[Constants.MAX_ENTITIES];
        }

        //Method add entity to world
        public int AddEntity(BaseEntity entity)
        {
            for(int i = 0; i < Constants.MAX_ENTITIES; i++)
            {
                if(m_entities[i] == null)
                {
                    m_entities[i] = entity;
                    return i;
                }
            }

            return -1;
        }

        public void RemoveEntity(int index)
        {
            m_entities[index] = null;
        }

        public void RemoveEntity(BaseEntity entity)
        {
            int index = Array.IndexOf(m_entities, entity);
            RemoveEntity(index);
        }
    }
}
