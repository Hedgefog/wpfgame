﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Shapes;

namespace WPFGame
{
    public class Render
    {
        public Point StartOrigin { get; set; }
        public Canvas Canvas { get; set; }

        public void ClearFrame()
        {
            if (this.Canvas == null)
                return;

            this.Canvas.Children.Clear();
        }

        /*--------[ Base ]--------*/

        public void DrawBox(Box box, SolidColorBrush color)
        {
            DrawLine(new Point(box.Left, box.Top), new Point(box.Right, box.Top), color);
            DrawLine(new Point(box.Right, box.Top), new Point(box.Right, box.Bottom), color);
            DrawLine(new Point(box.Right, box.Bottom), new Point(box.Left, box.Bottom), color);
            DrawLine(new Point(box.Left, box.Bottom), new Point(box.Left, box.Top), color);
        }

        public void DrawLine(Point point1, Point point2, SolidColorBrush color)
        {
            if (this.Canvas == null)
                return;

            //Setup line
            Line line = new Line();
            line.Stroke = color;
            line.StrokeThickness = 0.5;
            line.X1 = point1.X - this.StartOrigin.X;
            line.Y1 = -point1.Y + this.StartOrigin.Y;
            line.X2 = point2.X - this.StartOrigin.X;
            line.Y2 = -point2.Y + this.StartOrigin.Y;

            //Add element to canvas
            this.Canvas.Children.Add(line);
        }

        public void DrawImage(ImageSource imageSource, Point origin, double scale = 1)
        {
            if (this.Canvas == null)
                return;

            if (imageSource == null)
                return;

            //Setup image
            Image image = new Image();
            image.Source = imageSource;
            image.Width = image.Source.Width * scale;
            image.Height = image.Source.Height * scale;
            image.StretchDirection = StretchDirection.Both;
            image.Stretch = Stretch.Fill;

            //Setup position
            Point pos = new Point();
            pos.X = origin.X - image.Width / 2 - this.StartOrigin.X;
            pos.Y = -origin.Y - image.Height / 2 + this.StartOrigin.Y;

            //Setup position
            Canvas.SetLeft(image, pos.X);
            Canvas.SetTop(image, pos.Y);

            //Add element to canvas
            this.Canvas.Children.Add(image);
        }

        public void DrawMessage(string text, Point pos, SolidColorBrush color, int size = 12)
        {
            if (this.Canvas == null)
                return;

            //Setup label
            Label label = new Label();
            label.Content = text;
            label.FontSize = size;
            label.Foreground = color;

            //Setup position
            Canvas.SetLeft(label, pos.X);
            Canvas.SetTop(label, pos.Y);

            //Add element to canvas
            this.Canvas.Children.Add(label);
        }

        /*--------[ Engine ]--------*/

        public void DrawEntity(BaseEntity entity)
        {
            if (entity.Sprite == null)
                return;

            DrawSprite(entity.Sprite, entity.SpriteFrame, entity.Origin);
        }

        public void DrawSprite(Sprite sprite, int frame, Point origin, int scale = 1)
        {
            ImageSource imageSource = sprite.GetFrame(frame).Source;
            DrawImage(imageSource, origin, scale);
        }

        /*--------[ DEBUG ]--------*/

        public void DrawAxis()
        {
            DrawLine(new Point(-512, 0), new Point(512, 0), Brushes.Red);
            DrawLine(new Point(0, 512), new Point(0, -512), Brushes.Green);
        }

        public void DrawCollisionBox(BaseEntity entity)
        {
            Box box = new Box(entity.Origin, entity.Size);
            DrawBox(box, Brushes.Red);
        }
    }
}
