﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Threading;
using System.Windows;
using System.IO;

namespace WPFGame
{
    public class Sprite
    {
        public struct Frame
        {
            public ImageSource Source;
            public int Duration;
        }

        enum PARSE_STAGE
        {
            STAGE_IMAGE = 0,
            STAGE_WIDTH,
            STAGE_HEIGHT,
            STAGE_X,
            STAGE_Y,
            STAGE_DURATION,

            STAGE_COUNT
        }

        protected List<Frame> m_frameList;

        public int FrameCount
        {
            get { return m_frameList.Count; }
        }

        public Frame GetFrame(int frameIndex)
        {
            if (!IsValidFrame(frameIndex))
                return new Frame();

            return m_frameList[frameIndex];
        }

        public bool IsValidFrame(int frameIndex)
        {
            if (frameIndex < 0 || frameIndex >= m_frameList.Count)
                return false;

            return true;
        }

        public Sprite(string name = null)
        {
            m_frameList = new List<Frame>();

            if (name != null && load(name) == 1)
            {
                Console.Write("Error loading sprite.");
                return;
            }
        }

        public int load(string name)
        {
            try
            {
                using (StreamReader stream = new StreamReader("assets\\sprites\\" + name + ".spr"))
                {
                    string data = stream.ReadToEnd();

                    int frameCount = 0;

                    string frameImage = "";
                    Int32Rect frameRect = new Int32Rect();
                    int frameDuration = 0;

                    PARSE_STAGE stage = PARSE_STAGE.STAGE_IMAGE;

                    int valuePos = 0;
                    int valueLength = 0;

                    int length = data.Length;

                    for (int i = 0; i < length; i++)
                    {
                        char ch = data[i];

                        if (ch == ' ' || ch == '\t' || ch == '\n' || ch == '\r')
                        {
                            if (valueLength > 0)
                            {
                                string value = data.Substring(valuePos, valueLength);

                                switch (stage)
                                {
                                    case PARSE_STAGE.STAGE_IMAGE:
                                        frameImage = "assets\\textures\\" + value;
                                        break;
                                    case PARSE_STAGE.STAGE_WIDTH:
                                        frameRect.Width = Convert.ToInt32(value);
                                        break;
                                    case PARSE_STAGE.STAGE_HEIGHT:
                                        frameRect.Height = Convert.ToInt32(value);
                                        break;
                                    case PARSE_STAGE.STAGE_X:
                                        frameRect.X = Convert.ToInt32(value);
                                        break;
                                    case PARSE_STAGE.STAGE_Y:
                                        frameRect.Y = Convert.ToInt32(value);
                                        break;
                                    case PARSE_STAGE.STAGE_DURATION:
                                        frameDuration = Convert.ToInt32(value);
                                        break;
                                }

                                valueLength = 0;

                                stage++;

                                if (stage >= PARSE_STAGE.STAGE_COUNT)
                                {
                                    if (frameImage.Length <= 0)
                                        continue;

                                    BitmapImage image = new BitmapImage(new Uri(frameImage, UriKind.Relative));

                                    Frame frame;
                                    frame.Source = new CroppedBitmap(image, frameRect);
                                    frame.Duration = frameDuration;
                                    m_frameList.Add(frame);

                                    frameCount++;

                                    stage = PARSE_STAGE.STAGE_IMAGE;
                                }
                            }
                        }
                        else
                        {
                            if (valueLength == 0)
                                valuePos = i;

                            valueLength++;
                        }
                    }
                }
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.Message);
                return 1;
            }

            return 0;
        }
    }
}
