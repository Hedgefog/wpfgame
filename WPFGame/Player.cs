﻿using System;
using System.Windows;

namespace WPFGame
{
    public class Player : BaseCharacter
    {
        public event EventHandler EventKilled;

        public bool IsAlive { get; protected set; }
        public int Score { get; set; }

        public Player
        (
            Point origin = new Point(),
            Size size = new Size(),
            Vector velocity = new Vector(),
            Sprite sprite = null
        ) : base(origin, size, velocity, sprite)
        {
            this.MovementSpeed = 72;
            this.Name = "player";
            this.IsAlive = true;
        }

        public override void Touch(BaseEntity target)
        {
            if (target.Name == "zombie")
            {
                this.IsAlive = false;

                if (this.EventKilled != null)
                    this.EventKilled(this, EventArgs.Empty);
            }
        }
    }
}
